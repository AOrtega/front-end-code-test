import styled from "styled-components";

const DisplayStyle = styled.div`
    background-color: #202020;
    text-align: right;
    width: 400px;
`

const Value = styled.h1`
    color: white;
    font-size: xxx-large;
    padding: 36px 12px 12px 12px;
    margin: 0;
`

const Display = (props) => {
    return (
        <DisplayStyle>
            <Value>{props.value}</Value>
        </DisplayStyle>
    )
};

export default Display;
