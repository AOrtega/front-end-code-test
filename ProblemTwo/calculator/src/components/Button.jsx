import styled from 'styled-components';

const ButtonStyle = styled.button`
    font-size: xxx-large;
    width: 100px;
    padding: 24px 0;
    color: ${props => props.color || "black"};
    background-color: ${props => props.backgroundColor || "#C3C5C7"};
`

const Button = (props) => {
    return (
        <ButtonStyle onClick={props.onClick} color={props.color} backgroundColor={props.backgroundColor}>{props.value}</ButtonStyle>
    )
};

export default Button;
