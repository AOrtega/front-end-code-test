const ButtonRow = (props) => {
    const Button1 = props.button1;
    const Button2 = props.button2;
    const Button3 = props.button3;
    const Button4 = props.button4;

    return (
        <div>
            <Button1 value={props.value} />
            <Button2 value={props.value} />
            <Button3 value={props.value} />
            <Button4 value={props.value} />
        </div>
    )
};

export default ButtonRow;
