import { useEffect, useState } from 'react';
import styled from 'styled-components';
import Button from "./components/Button";
import ButtonRow from "./components/ButtonRow";
import Display from "./components/Display";

const ZeroButton = styled.button`
  font-size: xxx-large;
  width: 200px;
  padding: 24px 0;
  color: black;
  background-color: #CACACA;
`

function App() {
  const ACButton = () => <Button onClick={handleACButtonClick} value="AC" />
  const PlusMinusButton = () => <Button onClick={handlePlusMinusButtonClick} value="+/-" />
  const PercentButton = () => <Button onClick={handlePercentButtonClick} value="%" />
  const DivideButton = () => <Button onClick={() => handleOperandButtonClick('/')} color="white" backgroundColor="#F98D12" value="/" />
  const MultiplyButton = () => <Button onClick={() => handleOperandButtonClick('x')} color="white" backgroundColor="#F98D12" value="x" />
  const SubtractButton = () => <Button onClick={() => handleOperandButtonClick('-')} color="white" backgroundColor="#F98D12" value="-" />
  const AddButton = () => <Button onClick={() => handleOperandButtonClick('+')} color="white" backgroundColor="#F98D12" value="+" />
  const Button1 = () => <Button onClick={() => handleNumberButtonClick('1')} value="1" />
  const Button2 = () => <Button onClick={() => handleNumberButtonClick('2')} value="2" />
  const Button3 = () => <Button onClick={() => handleNumberButtonClick('3')} value="3" />
  const Button4 = () => <Button onClick={() => handleNumberButtonClick('4')} value="4" />
  const Button5 = () => <Button onClick={() => handleNumberButtonClick('5')} value="5" />
  const Button6 = () => <Button onClick={() => handleNumberButtonClick('6')} value="6" />
  const Button7 = () => <Button onClick={() => handleNumberButtonClick('7')} value="7" />
  const Button8 = () => <Button onClick={() => handleNumberButtonClick('8')} value="8" />
  const Button9 = () => <Button onClick={() => handleNumberButtonClick('9')} value="9" />
  const [number1, setNumber1] = useState('');
  const [number2, setNumber2] = useState('');
  const [value, setValue] = useState('');
  const [operand, setOperand] = useState('');
  
  useEffect(() => {
    var result = 0;

    if (operand === '/') {
      result = number1 / number2;
      setValue(result);
    } else if (operand === 'x') {
      result = number1 * number2;
      setValue(result);
    } else if (operand === '-') {
      result = number1 - number2;
      setValue(result);
    } else if (operand === '+') {
      result = +number1 + +number2
      setValue(result);
    }
  }, [number2]);

  function handleACButtonClick() {
    setNumber1('');
    setNumber2('');
    setValue('');
    setOperand('');
  }

  function handlePlusMinusButtonClick() {
    setValue(-value);
  }

  function handlePercentButtonClick() {
    setValue(.01 * value);
  }

  function handleOperandButtonClick(operand) {
    setNumber1(value);
    setOperand(operand);
    setValue('');
  }

  function handleNumberButtonClick(number) {
    setValue(value + number);
  }

  function handleEqualButtonClick() {
    setNumber2(value);
  }

  return (
    <div className="App">
      <Display value={value === '' ? '0' : value} />
      <div className="buttons">
        <ButtonRow button1={ACButton} button2={PlusMinusButton} button3={PercentButton} button4={DivideButton} />
        <ButtonRow button1={Button7} button2={Button8} button3={Button9} button4={MultiplyButton} />
        <ButtonRow button1={Button4} button2={Button5} button3={Button6} button4={SubtractButton} />
        <ButtonRow button1={Button1} button2={Button2} button3={Button3} button4={AddButton} />
        <div>
          <ZeroButton onClick={() => handleNumberButtonClick('0')}>0</ZeroButton>
          <Button onClick={() => handleNumberButtonClick('.')} value="." />
          <Button onClick={handleEqualButtonClick} color="white" backgroundColor="#F98D12" value="=" />
        </div>
      </div>
    </div>
  );
}

export default App;
