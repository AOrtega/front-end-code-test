var arr = [
  {
    'guest_type': 'crew',
    'first_name': 'Marco',
    'last_name': 'Burns',
    'guest_booking': {
        'room_no': 'A0073',
        'some_array': [7,2,4]
    },
  },
  {
    'guest_type': 'guest',
    'first_name': 'John',
    'last_name': 'Doe',
    'guest_booking': {
        'room_no': 'C73',
        'some_array': [1,3,5,2,4,3]
    },
  },
  {
    'guest_type': 'guest',
    'first_name': 'Jane',
    'last_name': 'Doe',
    'guest_booking': {
        'room_no': 'C73',
        'some_array': [1,3,5,2,4,3]
    },
  },
  {
    'guest_type': 'guest',
    'first_name': 'Albert',
    'last_name': 'Einstein',
    'guest_booking': {
        'room_no': 'B15',
        'some_array': [2,5,6,3]
    },
  },
  {
    'guest_type': 'crew',
    'first_name': 'Jack',
    'last_name': 'Daniels',
    'guest_booking': {
        'room_no': 'B15',
        'some_array': [2,5,6,3]
    },
  },
  {
    'guest_type': 'guest',
    'first_name': 'Alan',
    'last_name': 'Turing',
    'guest_booking': {
        'room_no': 'B15',
        'some_array': [2,5,6,3]
    },
  },
];

function mutateArray(a) {
    const result = a.filter(function(a) {
        // Step 1
        a.room_no = a.guest_booking.room_no;
        a.some_array = a.guest_booking.some_array;
        delete a.guest_booking;

        // Step 2
        var sum = 0;
        for (i = 0; i < a.some_array.length; i++) {
            sum = sum + a.some_array[i];
        }
        a.some_total = sum;
        delete a.some_array;

        // Step 3
        return a.guest_type == 'guest';
    });

    // Step 4
    return result.sort(compareLast);
}

function compareLast(a, b) {
    const lastNameA = a.last_name.toUpperCase();
    const lastNameB = b.last_name.toUpperCase();

    if (lastNameA < lastNameB) {
        return -1;
    } else if (lastNameA > lastNameB) {
        return 1;
    } else if (lastNameA == lastNameB) {
        return compareFirst(a, b);
    }

    return 0;
}

function compareFirst(a, b) {
    const firstNameA = a.first_name.toUpperCase();
    const firstNameB = b.first_name.toUpperCase();

    if (firstNameA < firstNameB) {
        return -1;
    } else if (firstNameA > firstNameB) {
        return 1;
    }

    return 0;
}

$(document).ready(function() {
    $('#originalArray').html(JSON.stringify(arr, null, 2));
    $('#resultsArray').html(JSON.stringify(mutateArray(arr), null, 2));
});
